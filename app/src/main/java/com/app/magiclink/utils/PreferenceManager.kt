package com.app.magiclink.utils

import com.pixplicity.easyprefs.library.Prefs

object PreferenceManager {

    var isFirstTime: Boolean
        get() = Prefs.getBoolean(PrefsKeys.IS_FIRST_TIME, true)
        set(value) = Prefs.putBoolean(PrefsKeys.IS_FIRST_TIME, value)


    fun clear() {
        Prefs.clear()
    }

}