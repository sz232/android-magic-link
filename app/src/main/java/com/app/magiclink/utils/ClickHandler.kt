package com.app.magiclink.utils

import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View

class ClickHandler(
    private val handler: () -> Unit,
    private val drawUnderline: Boolean
) : ClickableSpan() {

    override fun updateDrawState(ds: TextPaint) {
        if (drawUnderline) {
            super.updateDrawState(ds)
        } else {
            ds.isUnderlineText = false
        }
    }

    override fun onClick(widget: View) {
        handler()
    }
}