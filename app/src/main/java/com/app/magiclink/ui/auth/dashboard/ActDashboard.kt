package com.app.magiclink.ui.auth.dashboard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.magiclink.R
import com.app.magiclink.databinding.ActAuthBinding
import com.app.magiclink.databinding.ActDashboardBinding
import com.app.magiclink.extensions.addFragmentToAct
import com.app.magiclink.ui.auth.login.FragLogin
import com.app.magiclink.ui.base.ActBase

class ActDashboard : ActBase<ActDashboardBinding>() {

    override fun getViewBinding(): ActDashboardBinding {
        return ActDashboardBinding.inflate(layoutInflater)
    }

    override fun getCurContext() = this

    override fun onSetupView() {

    }
}