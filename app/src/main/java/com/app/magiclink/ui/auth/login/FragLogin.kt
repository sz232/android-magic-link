package com.app.magiclink.ui.auth.login

import android.graphics.Typeface
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.StyleSpan
import android.widget.TextView
import com.app.magiclink.R
import com.app.magiclink.databinding.FragLoginBinding
import com.app.magiclink.extensions.addFragmentToAct
import com.app.magiclink.ui.auth.signup.FragSignup
import com.app.magiclink.ui.base.FragBase
import com.app.magiclink.utils.ClickHandler
import kotlinx.android.synthetic.main.frag_login.view.*

class FragLogin : FragBase<FragLoginBinding>() {

    override fun getViewBinding(): FragLoginBinding {
        return FragLoginBinding.inflate(layoutInflater)
    }

    override fun onSetupView() {
        setSignupSpan(binding.tvSignup, "Signup", {
            activity?.addFragmentToAct(FragSignup(), R.id.authContainer)
        })
    }

    private fun setSignupSpan(textView: TextView, subString: String, handler: () -> Unit, drawUnderline: Boolean = false) {
        val text = textView.text
        val start = text.indexOf(subString)
        val end = start + subString.length

        val span = SpannableString(text)
        span.setSpan(ClickHandler(handler, drawUnderline), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        span.setSpan(StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        textView.linksClickable = true
        textView.isClickable = true
        textView.movementMethod = LinkMovementMethod.getInstance()

        textView.text = span
    }

    override fun setupObservers() {

    }
}