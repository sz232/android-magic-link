package com.app.magiclink.ui.boarding

import android.content.Intent
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.app.magiclink.R
import com.app.magiclink.adapter.AdapterBoarding
import com.app.magiclink.databinding.ActBoardingBinding
import com.app.magiclink.ui.auth.ActAuth
import com.app.magiclink.ui.base.ActBase
import com.app.magiclink.utils.PreferenceManager
import kotlinx.android.synthetic.main.act_boarding.view.*

data class Boarding(
    var boardImage: Int = 0,
    var boardText: String = "",
    var boardIndicator: Int = 0,
)

class ActBoarding : ActBase<ActBoardingBinding>() {

    private var pos: Int = 0
    private var boardingList: ArrayList<Boarding> = arrayListOf()
    private var adapterBoardingPager: AdapterBoarding? = null

    override fun getViewBinding(): ActBoardingBinding {
        return ActBoardingBinding.inflate(layoutInflater)
    }

    override fun getCurContext() = this

    override fun onSetupView() {
        bindBoardingPager()
    }

    private fun bindBoardingPager() {
        addBoardList()
        adapterBoardingPager = AdapterBoarding(context, boardingList)
        binding.vpBoarding?.apply {
            adapter = adapterBoardingPager
            addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    pos = position
                    updateView()
                }

                override fun onPageScrollStateChanged(state: Int) {
                }
            })
            offscreenPageLimit = boardingList.size
        }
        updateView()
    }

    private fun updateView() {
        when (pos) {
            0 -> {
                binding.ivIndicator.setImageResource(R.drawable.dot_one)
            }
            1 -> {
                binding.ivIndicator.setImageResource(R.drawable.dot_two)
            }
            2 -> {
                binding.ivIndicator.setImageResource(R.drawable.dot_three)
            }
        }
    }

    private fun addBoardList() {
        boardingList.add(
            Boarding(
                R.drawable.board_one,
                "",
                R.drawable.dot_one
            )
        )
        boardingList.add(
            Boarding(
                R.drawable.board_two,
                "",
                R.drawable.dot_two
            )
        )
        boardingList.add(
            Boarding(
                R.drawable.board_three,
                "",
                R.drawable.dot_three
            )
        )
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.tvNext -> {
                if (pos == 2) {
                    PreferenceManager.isFirstTime = false
                    startActivity(Intent(context, ActAuth::class.java))
                    finish()
                } else {
                    binding.vpBoarding.setCurrentItem(binding.vpBoarding.currentItem + 1, true)
                }
            }
            R.id.tvSkip -> {
                PreferenceManager.isFirstTime = false
                startActivity(Intent(context, ActAuth::class.java))
                finish()
            }
        }
    }

}