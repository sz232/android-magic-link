package com.app.magiclink.ui.auth.signup

import android.graphics.Typeface
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.StyleSpan
import android.widget.TextView
import com.app.magiclink.databinding.FragSignupBinding
import com.app.magiclink.ui.base.FragBase
import com.app.magiclink.utils.ClickHandler
import kotlinx.android.synthetic.main.frag_signup.view.*

class FragSignup : FragBase<FragSignupBinding>() {

    override fun getViewBinding(): FragSignupBinding {
        return FragSignupBinding.inflate(layoutInflater)
    }

    override fun onSetupView() {
        setSignInSpan(binding.tvSignin, "Signin", {
            parentFragmentManager.popBackStack()
        })

        binding.ivBack.setOnClickListener {
            parentFragmentManager.popBackStack()
        }

    }

    override fun setupObservers() {

    }

    private fun setSignInSpan(textView: TextView, subString: String, handler: () -> Unit, drawUnderline: Boolean = false) {
        val text = textView.text
        val start = text.indexOf(subString)
        val end = start + subString.length

        val span = SpannableString(text)
        span.setSpan(ClickHandler(handler, drawUnderline), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        span.setSpan(StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        textView.linksClickable = true
        textView.isClickable = true
        textView.movementMethod = LinkMovementMethod.getInstance()

        textView.text = span
    }

}