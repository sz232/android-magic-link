package com.app.magiclink.ui.splash

import android.content.Intent
import com.app.magiclink.R
import com.app.magiclink.databinding.ActSplashBinding
import com.app.magiclink.ui.auth.ActAuth
import com.app.magiclink.ui.base.ActBase
import com.app.magiclink.ui.boarding.ActBoarding
import com.app.magiclink.utils.PreferenceManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.act_splash.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ActSplash : ActBase<ActSplashBinding>() {

    override fun getViewBinding(): ActSplashBinding {
        return ActSplashBinding.inflate(layoutInflater)
    }

    override fun getCurContext() = this

    override fun onSetupView() {
        Glide.with(context)
            .load(R.drawable.ic_launcher_foreground)
            .circleCrop()
            .into(binding.ivSplashLogo)
        CoroutineScope(Dispatchers.Main).launch {
            delay(3000)

            if (PreferenceManager.isFirstTime) {
                startActivity(Intent(context, ActBoarding::class.java))
            } else {
                startActivity(Intent(context, ActAuth::class.java))
            }

            finish()
        }
    }

}