package com.app.magiclink.ui.base

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding


abstract class ActBase<B : ViewBinding> : AppCompatActivity() {

    lateinit var binding: View
    lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getViewBinding().root
        context = this
        setContentView(binding)
        onSetupView()
    }

    abstract fun getViewBinding(): B

    abstract fun getCurContext(): Context

    abstract fun onSetupView()



}