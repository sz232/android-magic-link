package com.app.magiclink.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding


abstract class FragBase<B : ViewBinding> : Fragment() {
    lateinit var binding: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = getViewBinding().root
        return binding
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onSetupView()
        setupObservers()
    }

    abstract fun getViewBinding(): B

    abstract fun onSetupView()

    abstract fun setupObservers()

}