package com.app.magiclink.ui.auth

import com.app.magiclink.R
import com.app.magiclink.databinding.ActAuthBinding
import com.app.magiclink.extensions.addFragmentToAct
import com.app.magiclink.ui.auth.login.FragLogin
import com.app.magiclink.ui.base.ActBase

class ActAuth : ActBase<ActAuthBinding>() {

    override fun getViewBinding(): ActAuthBinding {
        return ActAuthBinding.inflate(layoutInflater)
    }

    override fun getCurContext() = this

    override fun onSetupView() {
        addFragmentToAct(FragLogin(), R.id.authContainer, false)
    }



}