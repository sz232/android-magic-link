package com.app.magiclink.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.app.magiclink.databinding.PagerBoardBinding
import com.app.magiclink.ui.boarding.Boarding
import kotlinx.android.synthetic.main.pager_board.view.*

class AdapterBoarding(
    private val context: Context,
    private val boardList: ArrayList<Boarding>,
) : PagerAdapter() {

    override fun getCount() = boardList.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = PagerBoardBinding.inflate(LayoutInflater.from(container.context)).root

        view.ivBoard.setImageResource(boardList[position].boardImage)
        container.addView(view)
        return view
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

}