package com.app.magiclink.app

import android.app.Application
import android.content.ContextWrapper
import androidx.appcompat.app.AppCompatDelegate
import com.pixplicity.easyprefs.library.Prefs

class MagicLinkApp : Application() {

    companion object {
        private var instance: MagicLinkApp? = null
        fun getInstance(): MagicLinkApp? {
            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }

}