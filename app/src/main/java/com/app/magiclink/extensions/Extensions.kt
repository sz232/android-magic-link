package com.app.magiclink.extensions

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.app.magiclink.R
import com.app.magiclink.ui.base.ActBase


private var toast: Toast? = null
private var progressDialog: Dialog? = null

/*fun <T> apiErrorResponse(response: Response<T>): String {
    val error = Gson().fromJson(response.errorBody()?.charStream()?.readText(), ErrorResponse::class.java)
    return if (error.errors.providerType.isEmpty()) {
        error.message
    } else {
        error.errors.providerType[0]
    }
}*/

fun Activity.showToast(message: String) {
    toast(this, message)
}

fun Fragment.showToast(message: String) {
    toast(activity!!, message)
}

private fun toast(activity: Activity, message: String) {
    activity.runOnUiThread {
        if (message.isNotEmpty()) {
            if (toast != null) {
                toast?.cancel()
            }
//            toast = CustomToast(activity, status)
            toast = Toast.makeText(activity, message, Toast.LENGTH_LONG)
            toast?.show()
        }
    }
}

fun Activity.showLoader() {

    progressDialog.let {

        if (it?.isShowing == true) {
            it.dismiss()
        }

        progressDialog = Dialog(this)
        progressDialog?.setContentView(R.layout.dialog_loader)
        progressDialog?.window?.setGravity(Gravity.CENTER)
        progressDialog?.window?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        progressDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog?.setCancelable(false)
        progressDialog?.show()
    }

    Log.e("loader", "showLoader: ")
}

fun Activity.hideLoader() {
    try {
        if (!(this as ActBase<*>).isFinishing && !this.isDestroyed) {
            if (progressDialog?.isShowing!!) {
                progressDialog?.dismiss()
                Log.e("loader", "dismiss: ")
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.inVisible() {
    this.visibility = View.INVISIBLE
}