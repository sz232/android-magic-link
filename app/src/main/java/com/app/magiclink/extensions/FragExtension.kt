package com.app.magiclink.extensions

import android.app.Activity
import androidx.fragment.app.Fragment
import com.app.magiclink.R
import com.app.magiclink.ui.base.ActBase

fun Activity.replaceFragmentToAct(
    fragment: Fragment,
    container: Int,
    addToBackStack: Boolean = false,
    isAnimation: Boolean = true
) {
    val ft = (this as ActBase<*>).supportFragmentManager.beginTransaction()
    if (isAnimation)
        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_left)
    if (addToBackStack)
        ft.addToBackStack(fragment::class.java.name)
    ft.replace(container, fragment, "outer_fragment")
    ft.commit()
}

fun Activity.addFragmentToAct(
    fragment: Fragment,
    container: Int,
    addToBackStack: Boolean = true,
    isAnimation: Boolean = true
) {
    val ft = (this as ActBase<*>).supportFragmentManager.beginTransaction()
    if (addToBackStack)
        ft.addToBackStack(fragment::class.java.name)
    if (isAnimation)
        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_left)
    ft.add(container, fragment, "outer_fragment")
    ft.commit()
}


fun Fragment.addInnerFragment(
    fragment: Fragment,
    container: Int,
    addToBackStack: Boolean = true,
    isAnimation: Boolean = true
) {
    val ft = this.parentFragmentManager.beginTransaction()
    if (addToBackStack)
        ft.addToBackStack(fragment::class.java.name)
    if (isAnimation)
        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_left)
    ft.add(container, fragment, "inner_fragment")
    ft.commit()
}

fun Fragment.replaceInnerFragment(
    fragment: Fragment,
    container: Int,
    addToBackStack: Boolean = true,
    isAnimation: Boolean = true
) {
    val ft = this.parentFragmentManager.beginTransaction()
    if (addToBackStack)
        ft.addToBackStack(fragment::class.java.name)
    if (isAnimation)
        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_left)
    ft.replace(container, fragment, "inner_fragment")
    ft.commit()
}